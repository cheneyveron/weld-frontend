FROM node:4
EXPOSE 3000-3020
RUN mkdir -p /usr/src/app
WORKDIR /usr/src/app
COPY package.json /usr/src/app
COPY . /usr/src/app
RUN npm install -g cnpm --registry=https://registry.npm.taobao.org
RUN cnpm install
CMD [ "gulp", "serve" ]
