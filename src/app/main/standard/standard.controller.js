(function ()
{
    'use strict';

    angular
        .module('app.standard')
        .controller('StandardController', StandardController);

    /** @ngInject */
    function StandardController(DashboardData)
    {
        var vm = this;

        // Data Demo
        /*


        */

        vm.data = {
                "title": "焊接标准",
                "table": {
                    "rows": [
                        {
                            "name"  : "名字1",
                            "usage" : "用途1",
                            "source": "来源1",
                            "year"  : "年份1",
                            "url"   : "/upload/a.pdf",//文件地址
                            "level" : "1",//级别
                            "father": []//父类
                        },
                        {
                            "name"  : "名字2",
                            "usage" : "用途1",
                            "source": "来源1",
                            "year"  : "年份1",
                            "url"   : "/upload/a.pdf",//文件地址
                            "level" : "2",//级别
                            "father": [
                                "名字1"
                            ]//父类
                        },
                        {
                            "name"  : "名字3",
                            "usage" : "用途1",
                            "source": "来源1",
                            "year"  : "年份1",
                            "url"   : "/upload/a.pdf",//文件地址
                            "level" : "3",//级别
                            "father": [
                                "名字1",
                                "名字2"
                            ]//父类
                        },
                        {
                            "name"  : "名字4",
                            "usage" : "用途1",
                            "source": "来源1",
                            "year"  : "年份1",
                            "url"   : "/upload/a.pdf",//文件地址
                            "level" : "2",//级别
                            "father": [
                                "名字1"
                            ]//父类
                        }
                    ]
                }
        };

        // Widget 11
        vm.widget11 = {
            rows : vm.data.table.rows,
            dtOptions: {
                dom       : '<"top"f>rt<"bottom"<"left"<"length"l>><"right"<"info"i><"pagination"p>>>',
                pagingType: 'simple',
                autoWidth : false,
                responsive: true,
                //order     : [0, 'asc'],
                columnDefs: [
                    {
                        width  : '33%',
                        targets: [0]
                    },
                    {
                        orderable : false,
                        targets: [4,5]
                    },
                    {
                        orderable : true,
                        targets: [0,1,2,3]
                    }
                ]
            }
        };

        //vm.widget11.rows = {"1":2};

        //console.log(vm.widget11.rows[0]['name']);

    }

})();
