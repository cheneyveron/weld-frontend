(function ()
{
    'use strict';

    angular
        .module('app.standard', [])
        .config(config);

    /** @ngInject */
    function config($stateProvider, msApiProvider, msNavigationServiceProvider)
    {
        // State
        $stateProvider.state('app.standard', {
            url      : '/standard',
            views    : {
                'content@app': {
                    templateUrl: 'app/main/standard/standard.html',
                    controller : 'StandardController as vm'
                }
            },
            resolve  : {
                DashboardData: function (msApi)
                {
                    return msApi.resolve('app.standard@get');
                }
            },
            bodyClass: 'standard'
        });

        // Api
        msApiProvider.register('app.standard', ['app/data/dashboard/project/data.json']);


        msNavigationServiceProvider.saveItem('standard', {
          title : '焊接标准',
          icon  : 'icon-apple-safari',
          state : 'app.standard',
          weight: 1
        });
    }

})();
