(function ()
{
    'use strict';

    angular
        .module('app.material', [])
        .config(config);

    /** @ngInject */
    function config($stateProvider, msApiProvider, msNavigationServiceProvider)
    {
        // State
        $stateProvider.state('app.material', {
            url      : '/material',
            views    : {
                'content@app': {
                    templateUrl: 'app/main/material/material.html',
                    controller : 'MaterialController as vm'
                }
            },
            resolve  : {
                DashboardData: function (msApi)
                {
                    return msApi.resolve('app.material@get');
                }
            },
            bodyClass: 'material'
        });

        // Api
        msApiProvider.register('app.material', ['app/data/dashboard/analytics/data.json']);

        msNavigationServiceProvider.saveItem('material', {
          title : '焊接材料',
          icon  : 'icon-basket-fill',
          state : 'app.material',
          weight: 1
        });
    }

})();
