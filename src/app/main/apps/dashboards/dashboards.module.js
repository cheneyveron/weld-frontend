(function ()
{
    'use strict';

    angular
        .module('app.dashboards', [
            'app.dashboards.server',
            'app.dashboards.analytics'
        ])
        .config(config);

    /** @ngInject */
    function config(msNavigationServiceProvider)
    {
        // Navigation
        /*msNavigationServiceProvider.saveItem('apps', {
            title : 'APPS',
            group : true,
            weight: 1
        });*/

        msNavigationServiceProvider.saveItem('dashboards', {
            title : 'Dashboards',
            icon  : 'icon-tile-four',
            weight: 1
        });

        msNavigationServiceProvider.saveItem('dashboards.server', {
            title: 'Server',
            state: 'app.dashboards_server'
        });

        msNavigationServiceProvider.saveItem('dashboards.analytics', {
            title: 'Analytics',
            state: 'app.dashboards_analytics'
        });
    }

})();
