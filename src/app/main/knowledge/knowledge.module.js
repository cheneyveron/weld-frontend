(function ()
{
    'use strict';

    angular
        .module('app.knowledge', [])
        .config(config);

    /** @ngInject */
    function config($stateProvider, msApiProvider, msNavigationServiceProvider)
    {
        // State
        $stateProvider.state('app.knowledge', {
            url      : '/knowledge',
            views    : {
                'content@app': {
                    templateUrl: 'app/main/knowledge/knowledge.html',
                    controller : 'KnowledgeController as vm'
                }
            },
            resolve  : {
                DashboardData: function (msApi)
                {
                    return msApi.resolve('app.knowledge@get');
                }
            },
            bodyClass: 'knowledge'
        });

        // Api
        msApiProvider.register('app.knowledge', ['app/data/dashboard/server/data.json']);

        msNavigationServiceProvider.saveItem('knowledge', {
          title : '焊接基础知识',
          icon  : 'icon-appnet',
          state : 'app.knowledge',
          weight: 1
        });
    }

})();
