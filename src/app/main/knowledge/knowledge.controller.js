(function ()
{
    'use strict';

    angular
        .module('app.knowledge')
        .controller('KnowledgeController', KnowledgeController);

    /** @ngInject */
    function KnowledgeController($scope, $interval, DashboardData)
    {
        var vm = this;

        // Data
        vm.DashboardData = {
            "col1": {
                "title": "焊接标准",
                "table": {
                    "rows": [
                        {
                            "name": "名字1",
                            "url": "/upload/a.pdf",//文件地址
                            "level": "1",//级别
                            "father": []//父类
                        },
                        {
                            "name": "名字2",
                            "url": "/upload/a.pdf",//文件地址
                            "level": "2",//级别
                            "father": [
                                "名字1"
                            ]//父类
                        },
                        {
                            "name": "名字3",
                            "url": "/upload/a.pdf",//文件地址
                            "level": "3",//级别
                            "father": [
                                "名字1",
                                "名字2"
                            ]//父类
                        },
                        {
                            "name": "名字4",
                            "url": "/upload/a.pdf",//文件地址
                            "level": "2",//级别
                            "father": [
                                "名字1"
                            ]//父类
                        }
                    ]
                }
            },
            "col2": {
                "title": "焊接标准",
                "table": {
                    "rows": [
                        {
                            "name": "名字1",
                            "url": "/upload/a.pdf",//文件地址
                            "level": "1",//级别
                            "father": []//父类
                        },
                        {
                            "name": "名字2",
                            "url": "/upload/a.pdf",//文件地址
                            "level": "2",//级别
                            "father": [
                                "名字1"
                            ]//父类
                        },
                        {
                            "name": "名字3",
                            "url": "/upload/a.pdf",//文件地址
                            "level": "3",//级别
                            "father": [
                                "名字1",
                                "名字2"
                            ]//父类
                        },
                        {
                            "name": "名字4",
                            "url": "/upload/a.pdf",//文件地址
                            "level": "2",//级别
                            "father": [
                                "名字1"
                            ]//父类
                        }
                    ]
                }
            },
        };

        // Widget 11
        vm.widget11 = {
            dtOptions: {
                dom       : '<"top"f>rt<"bottom"<"left"<"length"l>><"right"<"info"i><"pagination"p>>>',
                pagingType: 'simple',
                autoWidth : false,
                responsive: true,
                order     : [0, 'asc'],
                columnDefs: [
                    {
                        width  : '33%',
                        targets: [0, 1]
                    },
                    {
                        orderable : false,
                        targets    :[0, 1 , 2]
                    }
                ]
            }
        };

    }
})();
