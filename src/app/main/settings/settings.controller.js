(function ()
{
    'use strict';

    angular
        .module('app.settings')
        .controller('SettingsController', SettingsController);

    /** @ngInject */
    function SettingsController($mdDialog, $scope, $mdMedia, $http)
    {
        var vm = this;

        // StandardData
        vm.standardData = {
            "title": "焊接标准",
            "table": {
                "rows": [
                    {
                        "id"    : 1,
                        "name"  : "名字1",
                        "usage" : "用途1",
                        "source": "来源1",
                        "year"  : "年份1",
                        "url"   : "/upload/a.pdf",//文件地址
                        "level" : "1",//级别
                        "fatherid":"",
                        "father": []//父类
                    },
                    {
                        "id"    : 2,
                        "name"  : "名字2",
                        "usage" : "用途1",
                        "source": "来源1",
                        "year"  : "年份1",
                        "url"   : "/upload/a.pdf",//文件地址
                        "level" : "2",//级别
                        "fatherid":1,
                        "father": [
                            "名字1"
                        ]//父类
                    },
                    {
                        "id"    : 3,
                        "name"  : "名字3",
                        "usage" : "用途1",
                        "source": "来源1",
                        "year"  : "年份1",
                        "url"   : "/upload/a.pdf",//文件地址
                        "level" : "3",//级别
                        "fatherid" : 2,//级别
                        "father": [
                            "名字1",
                            "名字2"
                        ]//父类
                    },
                    {
                        "id"    : 4,
                        "name"  : "名字4",
                        "usage" : "用途1",
                        "source": "来源1",
                        "year"  : "年份1",
                        "url"   : "/upload/a.pdf",//文件地址
                        "level" : "2",//级别
                        "fatherid" : 1,//级别
                        "father": [
                            "名字1"

                        ]//父类
                    }
                ]
            }
        };

        vm.allData = {
            col1:{
                "title": "焊接标准",
                "table": {
                    "rows": [
                        {
                            "id": 1,
                            "name": "名字1",
                            "url": "/upload/a.pdf",//文件地址
                            "level": "1",//级别
                            "fatherid":"",
                            "father": []//父类
                        },
                        {
                            "id": 2,
                            "name": "名字2",
                            "url": "/upload/a.pdf",//文件地址
                            "level": "2",//级别
                            "fatherid":"2",
                            "father": [
                                "名字1"
                            ]//父类
                        },
                        {
                            "id": 3,
                            "name": "名字3",
                            "url": "/upload/a.pdf",//文件地址
                            "level": "3",//级别
                            "fatherid":"2",
                            "father": [
                                "名字1",
                                "名字2"
                            ]//父类
                        },
                        {
                            "id": 4,
                            "name": "名字4",
                            "url": "/upload/a.pdf",//文件地址
                            "level": "2",//级别
                            "fatherid":"1",
                            "father": [
                                "名字1"
                            ]//父类
                        }
                    ]
                }
            },
            col2:{
                "title": "焊接标准",
                "table": {
                    "rows": [
                        {
                            "id": 1,
                            "name": "名字1",
                            "usage": "用途1",
                            "source": "来源1",
                            "year": "年份1",
                            "url": "/upload/a.pdf",//文件地址
                            "level": "1",//级别
                            "father": []//父类
                        },
                        {
                            "id": 2,
                            "name": "名字2",
                            "usage": "用途1",
                            "source": "来源1",
                            "year": "年份1",
                            "url": "/upload/a.pdf",//文件地址
                            "level": "2",//级别
                            "father": [
                                "名字1"
                            ]//父类
                        },
                        {
                            "id": 3,
                            "name": "名字3",
                            "usage": "用途1",
                            "source": "来源1",
                            "year": "年份1",
                            "url": "/upload/a.pdf",//文件地址
                            "level": "3",//级别
                            "father": [
                                "名字1",
                                "名字2"
                            ]//父类
                        },
                        {
                            "id": 4,
                            "name": "名字4",
                            "usage": "用途1",
                            "source": "来源1",
                            "year": "年份1",
                            "url": "/upload/a.pdf",//文件地址
                            "level": "2",//级别
                            "father": [
                                "名字1"
                            ]//父类
                        }
                    ]
                }
            }
        };

        // Widget 11
        vm.standard = {
            title : vm.standardData.title,
            table : vm.standardData.table,
            dtOptions: {
                dom       : '<"top"f>rt<"bottom"<"left"<"length"l>><"right"<"info"i><"pagination"p>>>',
                pagingType: 'simple',
                autoWidth : false,
                responsive: true,
                columnDefs: [
                    {
                        width  : '40%',
                        targets: [1]
                    },
                    {
                        orderable : false,
                        targets    :[5, 6, 7, 8]
                    },
                    {
                        orderable : true,
                        targets    :[0, 1 , 2, 3, 4]
                    }
                ]
            }
        };
        //添加
        vm.standard.add = {};

        // 其他
        vm.other = {
            dtOptions: {
                dom       : '<"top"f>rt<"bottom"<"left"<"length"l>><"right"<"info"i><"pagination"p>>>',
                pagingType: 'simple',
                autoWidth : false,
                responsive: true,
                columnDefs: [
                    {
                        width  : '40%',
                        targets: [1]
                    },
                    {
                        orderable : false,
                        targets    :[2, 3, 4, 5]
                    },
                    {
                        orderable : true,
                        targets    :[0, 1]
                    }
                ]
            }
        };

        console.log(123);

        //修改操作
        vm.standard.modify = function (ev, row) {
            console.log(row);

            var useFullScreen = ($mdMedia('sm') || $mdMedia('xs')) && $scope.customFullscreen;
            $mdDialog.show({
                templateUrl: '/app/main/settings/modify_standard_dialog.html',
                parent: angular.element(document.body),
                targetEvent: ev,
                clickOutsideToClose: true,
                fullscreen: useFullScreen,
                controller: DialogController,
                locals: {
                    row: row
                }
            });
            function DialogController($scope, $mdDialog, row) {
                $scope.row = row;
                $scope.hide = function () {
                    $mdDialog.hide();
                };

                $scope.cancel = function () {
                    $mdDialog.cancel();
                };

                $scope.answer = function (answer) {
                    $mdDialog.hide(answer);
                };

                row.confirmModify = function () {
                    var data = $.param(
                        {
                            no: row.no,
                            idno: row.idno,
                            time: row.time,
                            usertype: row.usertype,
                            workunit: row.workunit,
                            telephone: row.telephone,
                            mobilephone: row.mobilephone,
                            address: row.address,
                            postcode: row.postcode,
                            name: row.name,
                            email: row.email
                        }
                    );
                    $http({
                        method: 'POST',
                        url: "http://localhost:8088/backend/custommanage/add",
                        data: data,
                        headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                        withCredentials: true
                    }).then(
                        function (response) {
                            //成功函数
                            console.log(response);
                            if (response.data.id == 0) {
                                $mdDialog.show(
                                    $mdDialog.alert()
                                        .clickOutsideToClose(true)
                                        .parent(angular.element(document.body))
                                        .title("修改成功")
                                        .ok('关闭')
                                );
                            } else {
                                $mdDialog.show(
                                    $mdDialog.alert()
                                        .clickOutsideToClose(true)
                                        .parent(angular.element(document.body))
                                        .title("修改失败")
                                        .ok('关闭')
                                );
                            }
                        },
                        function (response) {
                            //失败函数
                            console.log(response);
                            $mdDialog.show(
                                $mdDialog.alert()
                                    .clickOutsideToClose(true)
                                    .parent(angular.element(document.body))
                                    .title('与服务器通讯失败!')
                                    .ok('关闭')
                            );
                        }
                    );
                    //dialog.confirmModify结束
                };
            }
        };

        //删除操作
        vm.standard.delete  = function (ev,row) {
            console.log(row.id);
            var confirm = $mdDialog.confirm()
                .title('您确定要删除吗')
                .ariaLabel('')
                .targetEvent(ev)
                .clickOutsideToClose(true)
                .parent(angular.element(document.body))
                .ok('删除')
                .cancel('不删除');
            $mdDialog.show(confirm).then(function() {
                var data = $.param({
                    id:row.id
                });
                $http({
                    method: 'POST',
                    url: "http://localhost:8088/backend/custommanage/delete",
                    data: data,
                    headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                    withCredentials: true
                }).then(
                    function (response) {
                        //成功函数
                        console.log(response);
                        if(response.data.id==0) {
                            $mdDialog.show(
                                $mdDialog.alert()
                                    .clickOutsideToClose(true)
                                    .parent(angular.element(document.body))
                                    .title('删除成功!')
                                    .ok('关闭')
                            );
                            location.reload();
                        }else{
                            $mdDialog.show(
                                $mdDialog.alert()
                                    .clickOutsideToClose(true)
                                    .parent(angular.element(document.body))
                                    .title(response.data.message)
                                    .ok('关闭')
                            );
                        }
                    },
                    function (response) {
                        //失败函数
                        console.log(response);
                        $mdDialog.show(
                            $mdDialog.alert()
                                .clickOutsideToClose(true)
                                .parent(angular.element(document.body))
                                .title('与服务器通讯失败!')
                                .ok('关闭')
                        );
                    }
                )
            }, function() {
            });
        };


        //修改操作
        vm.other.modify = function (ev, row) {
            console.log(row);

            var useFullScreen = ($mdMedia('sm') || $mdMedia('xs')) && $scope.customFullscreen;
            $mdDialog.show({
                templateUrl: '/app/main/settings/modify_other_dialog.html',
                parent: angular.element(document.body),
                targetEvent: ev,
                clickOutsideToClose: true,
                fullscreen: useFullScreen,
                controller: DialogController,
                locals: {
                    row: row
                }
            });
            function DialogController($scope, $mdDialog, row) {
                $scope.row = row;
                $scope.hide = function () {
                    $mdDialog.hide();
                };

                $scope.cancel = function () {
                    $mdDialog.cancel();
                };

                $scope.answer = function (answer) {
                    $mdDialog.hide(answer);
                };

                row.confirmModify = function () {
                    var data = $.param(
                        {
                            no: row.no,
                            idno: row.idno,
                            time: row.time,
                            usertype: row.usertype,
                            workunit: row.workunit,
                            telephone: row.telephone,
                            mobilephone: row.mobilephone,
                            address: row.address,
                            postcode: row.postcode,
                            name: row.name,
                            email: row.email
                        }
                    );
                    $http({
                        method: 'POST',
                        url: "http://localhost:8088/backend/custommanage/add",
                        data: data,
                        headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                        withCredentials: true
                    }).then(
                        function (response) {
                            //成功函数
                            console.log(response);
                            if (response.data.id == 0) {
                                $mdDialog.show(
                                    $mdDialog.alert()
                                        .clickOutsideToClose(true)
                                        .parent(angular.element(document.body))
                                        .title("修改成功")
                                        .ok('关闭')
                                );
                            } else {
                                $mdDialog.show(
                                    $mdDialog.alert()
                                        .clickOutsideToClose(true)
                                        .parent(angular.element(document.body))
                                        .title("修改失败")
                                        .ok('关闭')
                                );
                            }
                        },
                        function (response) {
                            //失败函数
                            console.log(response);
                            $mdDialog.show(
                                $mdDialog.alert()
                                    .clickOutsideToClose(true)
                                    .parent(angular.element(document.body))
                                    .title('与服务器通讯失败!')
                                    .ok('关闭')
                            );
                        }
                    );
                    //dialog.confirmModify结束
                };
            }
        };

        //删除操作
        vm.other.delete  = function (ev,row) {
            console.log(row.id);
            var confirm = $mdDialog.confirm()
                .title('您确定要删除吗')
                .ariaLabel('')
                .targetEvent(ev)
                .clickOutsideToClose(true)
                .parent(angular.element(document.body))
                .ok('删除')
                .cancel('不删除');
            $mdDialog.show(confirm).then(function() {
                var data = $.param({
                    id:row.id
                });
                $http({
                    method: 'POST',
                    url: "http://localhost:8088/backend/custommanage/delete",
                    data: data,
                    headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                    withCredentials: true
                }).then(
                    function (response) {
                        //成功函数
                        console.log(response);
                        if(response.data.id==0) {
                            $mdDialog.show(
                                $mdDialog.alert()
                                    .clickOutsideToClose(true)
                                    .parent(angular.element(document.body))
                                    .title('删除成功!')
                                    .ok('关闭')
                            );
                            location.reload();
                        }else{
                            $mdDialog.show(
                                $mdDialog.alert()
                                    .clickOutsideToClose(true)
                                    .parent(angular.element(document.body))
                                    .title(response.data.message)
                                    .ok('关闭')
                            );
                        }
                    },
                    function (response) {
                        //失败函数
                        console.log(response);
                        $mdDialog.show(
                            $mdDialog.alert()
                                .clickOutsideToClose(true)
                                .parent(angular.element(document.body))
                                .title('与服务器通讯失败!')
                                .ok('关闭')
                        );
                    }
                )
            }, function() {
            });
        };
    }
})();
