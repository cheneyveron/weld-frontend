(function ()
{
    'use strict';

    angular
        .module('app.settings', [])
        .config(config);

    /** @ngInject */
    function config($stateProvider, msNavigationServiceProvider)
    {
        $stateProvider.state('app.settings', {
            url      : '/settings',
            views    : {
                'content@app': {
                    templateUrl: 'app/main/settings/settings.html',
                    controller : 'SettingsController as vm'
                }
            },
            bodyClass: 'settings'
        });


      msNavigationServiceProvider.saveItem('settings', {
        title : '系统管理',
        icon  : 'icon-account',
        state : 'app.settings',
        weight: 1
      });
    }

})();
